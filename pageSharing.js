var Capture = (function () {
	"use strict";

	var State = {};

	var requestInterval = function (fn, delay) {
		var requestAnimFrame = (function () {
				return (
					window.requestAnimationFrame ||
					function (callback, element) {
						window.setTimeout(callback, 1000 / 60);
					}
				);
			})(),
			start = new Date().getTime(),
			handle = {};
		function loop() {
			handle.value = requestAnimFrame(loop);
			var current = new Date().getTime(),
				delta = current - start;
			if (delta >= delay) {
				fn.call();
				start = new Date().getTime();
			}
		}
		handle.value = requestAnimFrame(loop);
		return handle;
	};

	const capture = (selector = "[data-wrapper]") => {
		let el = document.querySelector(selector);
		html2canvas(el).then(function (canvas) {
			// document.body.appendChild(canvas);
			var myImage = canvas.toDataURL("image/png");
			document.querySelector("#viewport").src = myImage;
			console.log(myImage);
			requestAnimationFrame(capture());
		});
	};

	const bindEvents = () => {
		document.body.addEventListener("dblclick", function (e) {
			capture();
		});
	};

	const init = () => {
		requestInterval(capture, 300);
	};

	return {
		init: init,
	};
})();

Capture.init();
