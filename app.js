// https://stackoverflow.com/questions/4667068/how-to-measure-a-time-spent-on-a-page

var Monitoring = (function () {
	"use strict";

	var State = {
		data: {
			timeSpent: 0,
		},
	};

	const updateTimeSpent = () => {
		let timeNow = window.performance.now();
		State.data.timeSpent = Math.round(timeNow / 1000);
		// console.clear();
		// console.table(State.data);
		requestAnimationFrame(updateTimeSpent);
	};

	const timeSpent = () => {
		updateTimeSpent();
	};

	const exportData = () => {
		console.info("Exporting data…", State.data);
		localStorage.setItem("monitoringData", JSON.stringify(State.data));
	};

	const bindEvents = () => {
		window.addEventListener("beforeunload", (event) => {
			event.preventDefault();
			exportData();
			// Chrome requires returnValue to be set.
			event.returnValue = "";
		});
	};

	const init = () => {
		// Get last data
		let lastData = localStorage.getItem("monitoringData");
		if (!!lastData) console.info("last data", JSON.parse(lastData));
		// setup event listeners
		bindEvents();
	};

	return {
		init: init,
		timeSpent: timeSpent,
	};
})();

// Monitoring.init();
// Monitoring.timeSpent();
